const mongoose=require('mongoose');
const validator=require('validator');
const { getWeekDayFromDate, isDateBetween, calculateHourDifference, changeTimeOfDate, decimalToTime } = require('../utils/util');
const RendezVous = require('./RendezVous');

const EmployeSchema= new mongoose.Schema({
    nom:{
        type:String,
        required:[true,"Nom obligatoire"],
        validate: {
            validator: function(value) {
              return value.trim() !== '';
            },
            message: 'Nom obligatoire'
          }
    },
    prenoms:{
        type:String,
        required:false
    },
    email:{
        type:String,
        required:[true,"Email obligatoire"],
        validate: {
            validator: function(value) {
              return value.trim() !== '' && validator.isEmail(value);
            },
            message:'Entrer une adresse email valide'
        }
    }
},{strict:false});

EmployeSchema.set('toJSON',{
  transform:(doc,ret)=>{
    delete ret.mdp;
    return ret;
  }
})

EmployeSchema.method('checkSchedule',function (dateDebut){
  let weekDay=getWeekDayFromDate(dateDebut);
  if (!this.horairesTravail) {
    return false;
  }
  let intervals=this.horairesTravail[weekDay];
  // console.log(weekDay,":",intervals);
  return intervals.length==2&&isDateBetween(dateDebut,intervals);
  /* let check=0;
  for (let i = 0; i < intervals.length; i++) {
    if (intervals[i].length==2&&isDateBetween(dateDebut,intervals[i])) {
      check+=1;
    }
  }
  return check===intervals.length; */
});

EmployeSchema.method('getRdvInBetween',async function getRdvInBetween(dateDebut,dateFin){
  try {
    const criteria={
      employe:this._id,
      date:{
        $gte:dateDebut,
        $lte:dateFin
      }
    }
    const rdvs=await RendezVous.find(criteria).select('_id date');
    return rdvs;
  } catch (error) {
    throw error;
  }
});

EmployeSchema.method('checkIfPlanningIsFree',function checkIfPlanningIsFree(dateDebut,dateFin){
  const rendezvous=this.getRdvInBetween(dateDebut,dateFin);
  if (rendezvous.length>=0) {
    return false;
  }
  return true;
});

EmployeSchema.method('isFree',function isFree(dateDebut,dateFin){
  return this.checkSchedule(dateDebut)&&this.checkIfPlanningIsFree(dateDebut,dateFin);
})

EmployeSchema.method('checkIfIsFree',function checkIfIsFree(dateDebut,dateFin){
   if(!(this.checkSchedule(dateDebut)&&this.checkIfPlanningIsFree(dateDebut,dateFin))){
    throw new Error(`${this.nom} ${this.prenoms} n'est pas disponible pour cette date`);
   }
})

EmployeSchema.method('calculAvgTimeWork',function calculAvgTimeWork(){
  let tempsTotal=0;
  let date=new Date();
  for (const key in this.horairesTravail) {
      if (this.horairesTravail[key].length===2) {
        tempsTotal+=calculateHourDifference(changeTimeOfDate(date,this.horairesTravail[key][0]),changeTimeOfDate(date,this.horairesTravail[key][1]))
      }
  }
  return tempsTotal/7;
})

EmployeSchema.statics.getTempsTravail=async function(){
  const employes=await Employe.find({
    delete:0
  }).select('horairesTravail _id nom prenoms').exec();
  const result=[]
  for (let i = 0; i < employes.length; i++) {
    const element = employes[i];
    let avgTimeWork=decimalToTime(element.calculAvgTimeWork());
    element.horairesTravail=undefined;
    delete element.horairesTravail;
    result.push({
      _id:element._id,
      nom:element.nom,
      prenoms:element.prenoms,
      avgTimeWork:avgTimeWork
    })
  }
  return result;
}

const Employe=mongoose.model('employes',EmployeSchema);

module.exports=Employe;