var nodemailer = require('nodemailer');
const {EMAIL,PASSWORD} = require('../env.js');
const Mailgen=require('mailgen');



let config={
    service:'gmail',
    auth: {
        user:EMAIL,
        pass:PASSWORD
    }
}

const transporter = nodemailer.createTransport(config);

function mailSend(userEmail,subject,message){
    let conf={
        from:EMAIL,
        to:userEmail,
        subject:subject,
        text:message
    };    
    transporter.sendMail(conf).then(()=>{
        console.log('mail envoyé');
    }).catch(error=>{
        console.log(error);
    })
}

exports.mailSend=mailSend;