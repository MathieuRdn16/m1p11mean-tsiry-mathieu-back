

const { getStorage, ref ,uploadBytesResumable, getDownloadURL } = require('firebase/storage')
const { auth } = require('../config/firebase.config')


async function uploadImage(file, quantity,dossier) {
    const storageFB = getStorage();

    if (quantity === 'single') {
        const dateTime = Date.now();
        const fileName = `${dossier}/${dateTime}`
        const storageRef = ref(storageFB, fileName)
        const metadata = {
            contentType: file.type,
        }
        const snapshot=await uploadBytesResumable(storageRef, file.buffer, metadata);
        return await getDownloadURL(snapshot.ref);
    }

    if (quantity === 'multiple') {
        for(let i=0; i < file.images.length; i++) {
            const dateTime = Date.now();
            const fileName = `${dossier}/${dateTime}`
            const storageRef = ref(storageFB, fileName)
            const metadata = {
                contentType: file.images[i].mimetype,
            }

            const saveImage = await Image.create({imageUrl: fileName});
            file.item.imageId.push({_id: saveImage._id});
            await file.item.save();

            await uploadBytesResumable(storageRef, file.images[i].buffer, metadata);

        }
        return
    }

}

async function uploadFile(files,dossier) {    
    let data=[];
    for(let t=0; t < files.length; t++){
    const file = {
        type: files[t].mimetype,
        buffer: files[t].buffer
    }
    try {
                
        const buildImage = await uploadImage(file, 'single',dossier);
        data.push(buildImage);
    } catch (err) {
        console.log(err);
    }
    }
    return data;    
}

exports.uploadFile = uploadFile;