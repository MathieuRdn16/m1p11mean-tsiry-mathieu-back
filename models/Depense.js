const mongoose=require('mongoose');
const { generateListMonths, mergeTwoArray, makeQueryDate, getDefaultDateFin } = require('../utils/util');
const { MOIS_FIELDS, DEPENSES_FIELDS, DEFAULT_DATE_DEBUT } = require('../utils/Constante');


const DepenseSchema=new mongoose.Schema({
    type:{
        type:Object,
        required:[true,"Type obligatoire"],
    },
    valeur:{
        type:Number,
        required:[true,'Valeur obligatoire'],
        validate:{
            validator:function(value){
                return value>=0;
            },
            message:'Valeur doit être supérieure à 0'
        }
    },
    date:{
        type:Date,
        required:[true,'Date obligatoire'],
    }
},{strict:false});



const getQueryAggregateDepenses=(datedebut,datefin)=>{
    let query=[
        {
            $match:{
                date:makeQueryDate(datedebut,datefin)
            }
        },
        {
            $group:{
                _id:{
                    mois: { $month: "$date" },
                    annee: { $year: "$date" },
                },
                depenses:{$sum:'$valeur'}
            },
        },{
            $project:{
                _id:0,
                mois:"$_id.mois",
                annee:"$_id.annee",
                depenses:1
            }
        },
    ]
    return query;
}

DepenseSchema.statics.getDepensesMois=async function(datedebut=DEFAULT_DATE_DEBUT,datefin=getDefaultDateFin()){
    const result=await Depense.aggregate(getQueryAggregateDepenses(datedebut,datefin));
    const listMonths=generateListMonths(datedebut,datefin,DEPENSES_FIELDS);
    return mergeTwoArray(listMonths,result,MOIS_FIELDS);
}

const Depense=mongoose.model('depenses',DepenseSchema);

module.exports=Depense;