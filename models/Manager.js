const mongoose=require('mongoose');
var validator=require('validator');

const ManagerSchema=new mongoose.Schema({
    nom:{
        type:String,
        required:[true,"Nom obligatoire"],
        validate: {
            validator: function(value) {
              return value.trim() !== '';
            },
            message: 'Nom obligatoire'
          }
    },
    prenoms:{
        type:String,
        required:false
    },
    email:{
        type:String,
        required:[true,"Email obligatoire"],
        validate: {
            validator: function(value) {
              return value.trim() !== '' && validator.isEmail(value);
            },
            message:'Entrer une adresse email valide'
        }
    }
},{strict:false});
const Manager=mongoose.model('managers',ManagerSchema);

module.exports=Manager;