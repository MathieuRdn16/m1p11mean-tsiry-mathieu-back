const mongoose=require('mongoose');
const Employe = require('./Employe');

const ServiceSchema=new mongoose.Schema({
    designation:{
        type:String, 
        required:[true,'Désignation obligation'],       
    },
    prix:{
        type:Number,
        required:[true,'Prix obligation'], 
        validate:{
            validator :function(value){
                return value>=0;                
            },
            message:'Prix doit être supérieure à 0'
        }
    },
    duree:{
        type:Number,
        required:[true,'Durée obligation'],
        validate:{
            validator :function(value){
                return value>=0;                
            },
            message:'Durée doit être supérieure à 0'
        }
    },
    commission:{
        type:Number,
        required:[true,'Commission obligation'],
        validate:{
            validator :function(value){
                return value>=0;                
            },
            message:'Commission doit être supérieure à 0'
        }

    },
    employes:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:'employes'
    }],
    images:{
        type:Array,
    }    
},{strict:false});

ServiceSchema.methods.getEmployes=async ()=>{
    try {
        const employes=await Employe.find({_id:{$in:this.employes}})
        return employes;
    } catch (error) {
        throw error;
    }
};

const Service=mongoose.model('services',ServiceSchema);

module.exports=Service;