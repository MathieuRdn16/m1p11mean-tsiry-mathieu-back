const mongoose=require('mongoose');

const PreferenceSchema=new mongoose.Schema({
    client:{
        type:String,
        required:[true,'Veuillez vous authentifier'],   
    },
    service:{
        type:String,
        required:[true,'Service obligatoire'],   
    },
    employe:{
        type:String,
        required:[true,'Veuillez vous authentifier'],   
    },
},{strict:false});

const Preference=mongoose.model('preferences',PreferenceSchema);

module.exports=Preference