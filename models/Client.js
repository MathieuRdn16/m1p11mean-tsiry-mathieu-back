const mongoose=require('mongoose');
const validator=require('validator');
const bcrypt=require('bcrypt');
const Constante=require('./../utils/Constante.js');

const ClientSchema=new mongoose.Schema({
    nom:{
        type:String,
        required:[true,"Nom obligatoire"]
    },
    prenoms:{
        type:String,
        required:false
    },
    email:{
        type:String,
        required:[true,'Email manquant'],
        validate:{
            validator:validator.isEmail,
            message:'{VALUE} n\'est pas un email valide'
        }
    },
    password:{
        type:String,
        required:[true,'Mot de passe obligatoire']
    },
    password2:{
        type:String,
        select:false,
        required:[true,'Veuillez resaisir votre mot de passe'],
    }
},{strict:false});

ClientSchema.pre('save',function(next){
    console.log(this.password," ",this.password2);
    if (!validator.equals(this.password,this.password2)) {
        let error=new mongoose.Error.ValidationError();
        error.addError('password2',new mongoose.Error.ValidatorError({
            message:"Les deux mots de passe ne correspondent pas"
        }))
        return next(error);
    }
    this.password=bcrypt.hashSync(this.password,Constante.SALT_ROUND);
    this.password2=undefined;
    next();
})

ClientSchema.set('toJSON', {
    transform: (doc, ret) => {
      delete ret.password;
      return ret;
    },
});

ClientSchema.methods.addRendezVous=(rdv)=>{
    if (!this.rendezvous) {
        this.rendezvous=[]
    }
    this.rendezvous.push(rdv);
    return this.bulkSave();
}

const Client=mongoose.model('clients',ClientSchema);

module.exports=Client;