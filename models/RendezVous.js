const mongoose=require('mongoose');
const Offre = require('./Offre');
const { ETAT_PAYE, CA_JOURS_FIELDS, JOURS_FIELDS, MOIS_FIELDS, RESERVATIONS_FIELDS, DEFAULT_DATE_DEBUT } = require('../utils/Constante');
const { generateListDays, mergeTwoArray, generateListMonths, makeQueryDate, getDefaultDateFin } = require('../utils/util');

const RendezVousSchema=new mongoose.Schema({
    employe:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'employes',
        required:[true,"Employé obligatoire"]
    },
    client:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'clients',
        required:[true,"Client obligatoire"],
    },
    date:{
        type:Date,
        required:[true,"Date obligatoire"],
        validate:{
            validator:(value)=>{
                return value>new Date();
            },
            message:"Le rendez-vous doit être dans une date antérieure à maintenant"
        }
    },
    service:{
        type:Object,
        required:[true,"Service obligatoire"]
    },
    etat:{
        type:Number,
    }
},{strict:false});

RendezVousSchema.method('fetchOffre',async function fetchOffre(){
    const query={
        debut:{
            $lte:this.date
        },
        fin:{
            $gte:this.date
        },
        services:{
            $elemMatch:{
                // _id:new mongoose.Types.ObjectId(this.service._id)
                designation:this.service.designation,
                commission:this.service.commission,
                prix:this.service.prix
            }
        }
    };
    console.log(query);
    const offres=await Offre.find(query).select('-services').exec();
    this.offres=offres;
    return offres;
});

RendezVousSchema.statics.findRdvWithOffres=async function(idRdv,populate_field='employe'){
    let rdv=await RendezVous.findById({_id:idRdv}).populate({
        path:populate_field,
        select:'nom prenoms _id'
    }).exec();
    const offres=await rdv.fetchOffre();
    let totalRemise=offres?.reduce((sum,currval)=>sum+currval.reduction,0)||0;
    let prixFinal=rdv.service.prix-(rdv.service.prix*totalRemise/100)
    let commission=prixFinal*rdv.service.commission/100;
    return {
        ...rdv.toObject(),
        offres:offres,
        totalReduction:totalRemise,
        prixFinal:prixFinal,
        commission:commission
    };
}

const getQueryAggregateCa=(datedebut,datefin,isJour=true)=>{
    let query=[
        {
            $match:{
                etat:{$gte:ETAT_PAYE},
                date:makeQueryDate(datedebut,datefin)
            }
        },
        {
            $group:{
                _id:{
                    mois: { $month: "$date" },
                    annee: { $year: "$date" },
                },
                ca:{$sum:'$montant'},
                commission:{$sum:'$commission'}
            },
        },{
            $project:{
                _id:0,
                mois:"$_id.mois",
                annee:"$_id.annee",
                ca:1,
                commission:1,
            }
        },
    ];
    if (isJour) {
        query[1].$group._id.jour= { $dayOfMonth: "$date" };
        query[2].$project.jour= "$_id.jour";
    }
    console.log(query[0]);
    return query;
}


RendezVousSchema.statics.getCaJours=async function(datedebut=DEFAULT_DATE_DEBUT,datefin=getDefaultDateFin()){
    const result=await RendezVous.aggregate(getQueryAggregateCa(datedebut,datefin));
    const defaultDays=generateListDays(datedebut,datefin,CA_JOURS_FIELDS);
    return mergeTwoArray(defaultDays,result,JOURS_FIELDS);
}

RendezVousSchema.statics.getCaMois=async function(datedebut=DEFAULT_DATE_DEBUT,datefin=getDefaultDateFin()){
    const result=await RendezVous.aggregate(getQueryAggregateCa(datedebut,datefin,isJour=false));
    const defaultMonths=generateListMonths(datedebut,datefin,CA_JOURS_FIELDS);
    return mergeTwoArray(defaultMonths,result,MOIS_FIELDS);
}

function getQueryAggregateReservations(datedebut,datefin,isJour=true){
    const query=[
        {
            $match:{
                date:makeQueryDate(datedebut,datefin)
            }
        },
        {
            $group: {
                _id: {
                    mois: { $month: "$date" },
                    annee: { $year: "$date" },
                },
                nbReservations:{$sum:1}
            }
        },{
            $project:{
                _id:0,
                mois:"$_id.mois",
                annee:"$_id.annee",
                nbReservations:1,
            }
        }
    ]
    if (isJour) {
        query[1].$group._id.jour= { $dayOfMonth: "$date" };
        query[2].$project.jour= "$_id.jour";
    }
    return query;
}

RendezVousSchema.statics.getReservationsJours=async function(datedebut=DEFAULT_DATE_DEBUT,datefin=getDefaultDateFin()){
    const result=await RendezVous.aggregate(getQueryAggregateReservations(datedebut,datefin));
    const defaultDays=generateListDays(datedebut,datefin,RESERVATIONS_FIELDS);
    return mergeTwoArray(defaultDays,result,JOURS_FIELDS);
}

RendezVousSchema.statics.getReservationsMois=async function(datedebut=DEFAULT_DATE_DEBUT,datefin=getDefaultDateFin()){
    const result=await RendezVous.aggregate(getQueryAggregateReservations(datedebut,datefin,false));
    const defaultDays=generateListMonths(datedebut,datefin,RESERVATIONS_FIELDS);
    return mergeTwoArray(defaultDays,result,MOIS_FIELDS);
}


const RendezVous=mongoose.model('rendezvous',RendezVousSchema);
module.exports=RendezVous;