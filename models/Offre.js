const mongoose=require('mongoose');

const OffreSchema=new mongoose.Schema({
    designation:{
        type:String,
        required:[true,'Désignation obligatoire'],
    },
    debut:{
        type:Date,
        required:[true,'Date de début obligatoire']
    },
    fin:{
        type:Date,
        required:[true,'Date de fin obligatoire']
    },
    services:{
        type:Array,
        required:[true,'Service obligatoire'],
        validate:{
            validator:function(value){
                return Array.isArray(value) && value.length > 0;
            },
            message: 'Service obligatoire'            
        }
    },
    prixTotal:{
        type:Number,
        required:[true,'Prix obligation'], 
        validate:{
            validator :function(value){
                return value>=0;                
            },
            message:'Prix doit être supérieure à 0'
        }
    },
    duree:{
        type:Number,
        required:[true,'durée obligation'], 
        validate:{
            validator :function(value){
                return value>=0;                
            },
            message:'Durée doit être supérieure à 0'
        }
    },
    commission:{
        type:Number,
        required:[true,'Commission obligation'], 
        validate:{
            validator :function(value){
                return value>=0;                
            },
            message:'Commission doit être supérieure à 0'
        }
    },
    reduction:{
        type:Number,
        required:[true,'Réduction obligation'], 
        validate:{
            validator :function(value){
                return value>=0 && value<=100;                
            },
            message:'Réduction doit être entre 1 et 100'
        }
    },
    prixReduit:{
        type:Number,
        required:[true,'Prix de réduction obligation'], 
        validate:{
            validator :function(value){
                return value>=0;
            },
            message:'Réduction doit être supérieure à 0'
        }
    }    
},{strict:false});

const Offre=mongoose.model('offres',OffreSchema);
 
module.exports=Offre;
