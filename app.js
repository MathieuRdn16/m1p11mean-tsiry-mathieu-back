var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var dbconnect=require('./utils/dbconnect');
const cors=require('cors');
var bodyParser=require('body-parser');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var clientsRouter = require('./routes/clients');
var managersRouter= require('./routes/managers');
var employesRouter= require('./routes/employes');
var servicesRouter= require('./routes/service');
var offreRouter= require('./routes/offre');
var catalogueRouter=require('./routes/catalogue');
var preferencesRouter= require('./routes/preference');
var typeDepenseRouter=require('./routes/typeDepense');
var depenseRouter=require('./routes/depense');
var statistiquesRouter=require('./routes/statistiques');
const ErrorException = require('./utils/ErrorException');
const { authMiddleware } = require('./utils/auth');
const { manageError } = require('./utils/util');
var task=require('./utils/task');
// process.env.TZ = 'Indian/Antananarivo';

dbconnect.dbconnect();

var app = express();



// view engine setup

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/clients', clientsRouter);
app.use('/managers', managersRouter);
app.use('/employes', employesRouter);
app.use('/services', servicesRouter);
app.use('/offres', offreRouter);
app.use('/catalogues', catalogueRouter);
app.use('/preferences',preferencesRouter);
app.use('/typeDepenses',typeDepenseRouter);
app.use('/depenses',depenseRouter);
app.use('/statistiques',statistiquesRouter);

const cron = require('node-cron');
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  console.log(err);
  res.status(err.status||500).json({message: manageError(err)});
});

const dailyTask = () => {
  task.rdvTomorrow();
  console.log('job')
};

cron.schedule('0 8 * * *', dailyTask);


module.exports = app;
