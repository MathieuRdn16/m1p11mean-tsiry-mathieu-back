const { initializeApp } = require("firebase/app");
const { getAuth } = require("firebase/auth");

  const firebaseConfig = {
    apiKey: "AIzaSyAEqlvoCxXllYFUa1l5oyFM9JVinuKPvFM",
    authDomain: "m1p11mean-tsiry-mathieu.firebaseapp.com",
    projectId: "m1p11mean-tsiry-mathieu",
    storageBucket: "m1p11mean-tsiry-mathieu.appspot.com",
    messagingSenderId: "284005354007",
    appId: "1:284005354007:web:25221d76ffa92811c4aa52",
    measurementId: "G-X2KJP3Q45J"
  };

  const app = initializeApp(firebaseConfig);
const auth = getAuth(app)

module.exports = {
  auth
}