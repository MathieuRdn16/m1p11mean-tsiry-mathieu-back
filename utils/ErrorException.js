function getMessageValidatorInsertion(err) {
    if (err.errors == undefined)
        return err;    
    if (err.errors.nom != undefined)
        return err.errors.nom.message;
    if (err.errors.email != undefined)
        return err.errors.email.message;
    if (err.errors.designation != undefined)
        return err.errors.designation.message;
    if (err.errors.prix != undefined)
        return err.errors.prix.message;
    if (err.errors.duree != undefined)
        return err.errors.duree.message;
    if (err.errors.commission != undefined)
        return err.errors.commission.message;
    if (err.errors.employes != undefined)
        return err.errors.employes.message;
    if (err.errors.images != undefined)
        return err.errors.images.message;
    if (err.errors.designation != undefined)
        return err.errors.designation.message;
    if (err.errors.debut != undefined)
        return err.errors.debut.message;
    if (err.errors.fin != undefined)
        return err.errors.fin.message;
    if (err.errors.services != undefined)
        return err.errors.services.message;
    if (err.errors.prixTotal != undefined)
        return err.errors.prixTotal.message;
    if (err.errors.reduction != undefined)
        return err.errors.reduction.message;
    if (err.errors.prixReduit != undefined)
        return err.errors.prixReduit.message;
    if(err.errors.type!=undefined)
        return err.errors.type.message;
    if(err.errors.valeur!=undefined)
        return err.errors.valeur.message;
    if(err.errors.date!=undefined)
        return err.errors.date.message;
    else return err.errors
}

exports.getMessageValidatorInsertion = getMessageValidatorInsertion;