const rdv=require('../models/RendezVous');
const MailSender=require('../models/MailSender');
const Client=require('../models/Client');

async function rdvTomorrow(){
    let date=new Date();
    let now=new Date();
    date.setUTCHours(0, 0, 0, 0);
    date.setDate(date.getDate() + 2);        
    let data=[];
    const query={date:{$gte:now,$lte:date}}
    let list=[]
    await rdv.find(query).then((data)=>{
        list=data;
    }).catch((error)=>{
        console.log(error) 
        return;       
        })    
    await sendMail(list);
}

async function sendMail(list){
    let client=[]
    await Client.find().then((data)=>{
        client=data;
    }).catch((error)=>{
        console.log(error)        
        return;
        })    
    for(let ls of list){
        const index=client.findIndex(obj=>obj._id.equals(ls.client));        
        if(index!==-1){
            let formattedTime = new Intl.DateTimeFormat('fr-FR', {
                hour: 'numeric',
                minute: 'numeric',
                second: 'numeric',
                hour12: false,
              }).format(ls.date);
            MailSender.mailSend(client[index].email,"Rappel rendez-vous",messageMail(formattedTime));            
        }        
    }
}

function messageMail(date){
    return `Bonjour votre rendez-vous pour demain sera à ${date}`
}

exports.rdvTomorrow=rdvTomorrow;