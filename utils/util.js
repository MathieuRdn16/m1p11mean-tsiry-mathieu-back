const mongoose = require('mongoose');
const JOURS_SEMAINE = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

function generatePayload(data = undefined, error = undefined) {
    return {
        data: data,
        message: manageError(error)
    }
}

function getDefaultSchedule() {
    let schedule = {};
    for (let index = 0; index < JOURS_SEMAINE.length; index++) {
        schedule[JOURS_SEMAINE[index]] = []
    }
    return schedule;
}

function getWeekDayFromDate(date) {
    let weekDay = new Date(date).getDay();
    let indice = (weekDay + 6) % 7;
    return JOURS_SEMAINE[indice];
}

function changeTimeOfDate(inputDate, timeString) {
    const date = new Date(inputDate);
    const [hours, minutes] = timeString.split(':').map(Number);
    date.setHours(hours);
    date.setMinutes(minutes);
    date.setHours(date.getHours());
    return date;
}

function isDateBetween(date, interval) {
    const [dateDebut, dateFin] = [changeTimeOfDate(date, interval[0]), changeTimeOfDate(date, interval[1])];
    return date >= dateDebut && date <= dateFin;
}

const manageError = (error) => {
    if (error instanceof mongoose.Error.ValidationError) {
        let errorMessages = '';
        const validationErrors = error.errors;
        for (const field in validationErrors) {
            if (validationErrors.hasOwnProperty(field)) {
                errorMessages += validationErrors[field].message;
            }
        }
        return errorMessages;
    }
    return error?.message;
};

const makeQueryRendezVous = (req, idUser) => {
    console.log('idUser=', idUser);
    let query = {
        client: new mongoose.Types.ObjectId(idUser),
        $or: [
            { 'employe.nom': makeQueryNom(req) },
            { 'employe.prenoms': makeQueryNom(req) }
        ],
    };
    if (req.query.dateMin) {
        query.date = { $gte: new Date(req.query.dateMin) };
    }
    if (req.query.dateMax) {
        query.date = {
            ...query.date,
            $lte: changeTimeOfDate(new Date(req.query.dateMax), '23:59')
        };
    }
    if (req.query.service) {
        query = {
            ...query,
            'service.designation': {
                $regex: new RegExp(req.query.service, 'i')
            }
        };
    }
    return query;
}
const makeQueryRendezVousEmploye = (req, idUser) => {
    console.log('idUser=', idUser);
    let query = {
        employe: new mongoose.Types.ObjectId(idUser),
        $or: [
            { 'client.nom': makeQueryNom(req) },
            { 'client.prenoms': makeQueryNom(req) }
        ],
    };
    if (req.query.dateMin) {
        query.date = { $gte: new Date(req.query.dateMin) };
    }
    if (req.query.dateMax) {
        query.date = {
            ...query.date,
            $lte: changeTimeOfDate(new Date(req.query.dateMax), '23:59')
        };
    }
    if (req.query.service) {
        query = {
            ...query,
            'service.designation': {
                $regex: new RegExp(req.query.service, 'i')
            }
        };
    }
    return query;
}

const makeQueryNom = (req) => {
    let nomEmp = req.query.employe ? req.query.employe : '';
    let nomSplits = nomEmp.split(' ');
    return {
        $in: nomSplits.map(nom => new RegExp(nom, 'i'))
    };
}

const generateListDays = (dateStart, dateEnd, fields) => {
    let result = [];
    let endDate = new Date(dateEnd);
    let currentDate = new Date(dateStart);
    while (currentDate <= endDate) {
        let day = {
            jour:currentDate.getDate(),
            mois:currentDate.getMonth()+1,
            annee:currentDate.getFullYear(),
        }
        fields.forEach(field => {
            day[field] = 0;
        });
        result.push(day);
        currentDate.setDate(currentDate.getDate() + 1);
    }

    return result;
}

function addOneMonth(date) {
    const newDate = new Date(date);
    newDate.setMonth(newDate.getMonth() + 1);
    if (newDate.getMonth() > 11) {
        newDate.setMonth(0);
        newDate.setFullYear(newDate.getFullYear() + 1);
    }

    return newDate;
}

const generateListMonths = (dateStart, dateEnd, fields) => {
    let result = [];
    let endDate = new Date(dateEnd);
    let currentDate = new Date(dateStart);
    while (currentDate <= endDate) {
        let day = {
            mois:currentDate.getMonth()+1,
            annee:currentDate.getFullYear(),
        }
        fields.forEach(field => {
            day[field] = 0;
        });
        result.push(day);
        currentDate=addOneMonth(currentDate);
    }

    return result;
}

const mergeTwoArray=(arr1,arr2,fieldsToCompare)=>{
    if (!Array.isArray(arr1) || !Array.isArray(arr2)) {
        throw new Error('Both arguments must be arrays');
    }

    return arr1.map(obj1 => {
        const matchingObj = arr2.find(obj2 => {
            return fieldsToCompare.every(field => obj1[field] === obj2[field]);
        });

        return matchingObj ? { ...obj1, ...matchingObj } : obj1;
    });
}

const calculBenefices=(ca,depenses,page=1,npp=12)=>{
    let results=[];
    for (let i = 0; i < ca.length; i++) {
        let totalDepenses=ca[i].commission+depenses[i].depenses;
        let benefice=ca[i].ca-totalDepenses;
        results.push({
            ...ca[i],
            ...depenses[i],
            totalDepenses:totalDepenses,
            benefice:benefice
        })
    }
    return paginateArray(results,page,npp);
}

function calculateHourDifference(date1, date2) {
    const time1 = date1.getTime();
    const time2 = date2.getTime();
  
    const timeDifference = Math.abs(time2 - time1);
  
    const hoursDifference = timeDifference / (1000 * 60 * 60);
  
    return hoursDifference;
}


function decimalToTime(decimalHours) {
    var hours = Math.floor(decimalHours);
    var minutes = Math.floor((decimalHours - hours) * 60);
    var seconds = Math.floor(((decimalHours - hours) * 60 - minutes) * 60);
    /* return {
        hours: hours,
        minutes: minutes,
        seconds: seconds
    }; */
    return `${hours}h ${minutes}min ${seconds}s`;
}

function paginateArray(data, page, npp) {
    const startIndex = (page - 1) * npp;
    const endIndex = startIndex + npp;
    const paginatedArray = data.slice(startIndex, endIndex);
    let totalPage=Math.floor(data.length/npp)+1;
    let ndata={
        data:paginatedArray,
        totalPage:totalPage,
        currentPage:page
    }
    return ndata;
}

function makeQueryDate(datedebut,datefin){
    let query={};
    if (datedebut) {
        query['$gte']=new Date(datedebut)
    }
    if (datefin) {
        query['$lte']=new Date(datefin)
    }
    return query;
}

function getDefaultDateFin(){
    return new Date();
}

module.exports = {
    generatePayload,
    getDefaultSchedule,
    getWeekDayFromDate,
    changeTimeOfDate,
    isDateBetween,
    manageError,
    makeQueryRendezVous,
    makeQueryRendezVousEmploye,
    makeQueryNom,
    generateListDays,
    mergeTwoArray,
    generateListMonths,
    calculBenefices,
    calculateHourDifference,
    decimalToTime,
    paginateArray,
    makeQueryDate,
    getDefaultDateFin
}