const jwt = require('jsonwebtoken');
const bcrypt=require('bcrypt');
const CONSTANTE=require('./Constante');
const { generatePayload } = require('./util');

function generateToken(user,userType=CONSTANTE.CLIENT) {
    const payload = {
        _id: user._id,
        nom: user.nom,
        email:user.email,
        prenoms:user.prenoms,
        userType:userType
    };
    const options = {
        expiresIn: '72h', 
    };
    console.log(payload)
    const token = jwt.sign(payload, CONSTANTE.JWT_SECRET_KEY, options);
    return token;
}

 function verifyToken(token){    
    try {
        let decode = jwt.verify(token.replace('Bearer ', ''), CONSTANTE.JWT_SECRET_KEY);
        return decode;
    } catch (err) {
        throw err;
    }
 }

function cryptage(mdp){
    return bcrypt.hashSync(mdp,CONSTANTE.SALT_ROUND);
}


function messageInscri(lien){
    return "Bonjour,\n\t"+
    "Veuillez suivre ce lien pour définir votre mots de passe :\n\t"+lien;
}

function messageOublieMdp(lien){
    return "Bonjour,\n\t"+
    "Veuillez suivre ce lien pour réinitialiser votre mots de passe :\n\t"+lien;
}

const authMiddleware=(req,res,next)=>{
    const token= req.headers['authorization'];     
    if(!token){
        return res.status(401).json({message:'Aucun token fourni'} );
    }
    const user=verifyToken(token);
    req.user=user;
    next();
}

function checkUserType(req,res,next,type,errorMessage){
    const user=req.user;
    if (type!==user.userType) {
        return
    }
}

const checkEmployeMiddleware=(req,res,next)=>{
    const user=req.user;
    if (user.userType!==CONSTANTE.EMPLOYE) {
        return res.status(401).json({message:'Action non-autorisé'});
    }
    next();
}

const checkManagerMiddleware=(req,res,next)=>{
    const user=req.user;    
    if (user.userType!==CONSTANTE.MANAGER) {
        return res.status(401).json({message:'Action non-autorisé'});
    }
    next();
}

const checkClientMiddleware=(req,res,next)=>{
    const user=req.user;
    if (user.userType!==CONSTANTE.CLIENT) {
        return res.status(401).json({message:'Action non-autorisé'});
    }
    next();
}
const footer="\n\t Ceci est un email automatique veuillez ne pas y répondre,";    
    
exports.generateToken = generateToken;
exports.verifyToken=verifyToken;
exports.cryptage=cryptage;
exports.messageInscri=messageInscri;
exports.messageOublieMdp=messageOublieMdp;
exports.authMiddleware=authMiddleware;
exports.checkEmployeMiddleware=checkEmployeMiddleware;
exports.checkClientMiddleware=checkClientMiddleware;
exports.checkManagerMiddleware=checkManagerMiddleware;
