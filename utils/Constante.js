const SALT_ROUND = 10;
const JWT_SECRET_KEY = 'm1p11mean';

const EMPLOYE = 'EMPLOYE';
const MANAGER = 'MANAGER';
const CLIENT = 'CLIENT';

const ETAT_CREE = 1;
const ETAT_VALIDE = 11;
const ETAT_PAYE = 9;
const ETAT_ANNULE = 0;
const CA_JOURS_FIELDS=['ca','commission'];
const DEPENSES_FIELDS=['depenses'];
const RESERVATIONS_FIELDS=['nbReservations'];
const JOURS_FIELDS=['jour','mois','annee'];
const MOIS_FIELDS=['mois','annee'];

const DEFAULT_DATE_DEBUT=new Date('2023-01-01');

module.exports = {
    SALT_ROUND,
    JWT_SECRET_KEY,
    EMPLOYE,
    MANAGER,
    CLIENT,
    ETAT_CREE,
    ETAT_VALIDE,
    ETAT_PAYE,
    ETAT_ANNULE,
    CA_JOURS_FIELDS,
    JOURS_FIELDS,
    MOIS_FIELDS,
    DEPENSES_FIELDS,
    RESERVATIONS_FIELDS,
    DEFAULT_DATE_DEBUT
};
