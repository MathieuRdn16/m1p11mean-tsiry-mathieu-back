const mongoose = require('mongoose');

const DATABASE = 'salon_beaute'
const MONGO_URL = `mongodb+srv://tpmean0302:q3sBnWdVW60Jj5uA@m1p11-tsiry-mathieu-db.vwszbsd.mongodb.net/${DATABASE}`;

function dbconnect() {
    mongoose.connect(MONGO_URL);

    const db = mongoose.connection;

    db.on('connected', () => {
        console.log(`Connected to MongoDB at ${MONGO_URL}`);
    });

    // Event listener for connection error
    db.on('error', (err) => {
        console.error(`MongoDB connection error: ${err}`);
    });

    // Event listener for disconnected
    db.on('disconnected', () => {
        console.warn('MongoDB disconnected');
    });
}

exports.dbconnect = dbconnect;