async function employeActif(service,employe){    
    for(let t in service){
        if(service[t].employes==undefined)
            continue;
        let tab=[];
        for(let i in service[t].employes){
            const index=employe.findIndex(obj=>obj._id==service[t].employes[i]);  
            if(index!==-1)
                tab.push(service[t].employes[i]);
        }
        service[t].employes=tab;
    }    
    return service;
}

exports.employeActif=employeActif;