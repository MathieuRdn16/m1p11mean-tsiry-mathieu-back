const Service = require('../models/Service');
const Employe = require('../models/Employe');
const Preference = require('../models/Preference');

async function listPreferenceDispo(idUser) {
    let employes ;
    await Employe.find({ 'delete': { $in: [0, null] } }).then((data) => {
        employes = data;
    }).catch((error) => {
        throw error;
    })
    let services ;
    await Service.find({ 'desactivate': { $in: [0, null] } }).then((data) => {
        services = data;
    }).catch((error) => {
        throw error;
    })
    let preferences;
    await Preference.find({ 'delete': { $in: [0, null] }, 'client': idUser }).then((data) => {
        preferences = data;
    }).catch((error) => {
        throw error;
    })
    const serviceEmploye = await listeServiceWithEmploye(services, employes);    
    let preferenceActif = [];
    for (let t in preferences) {
        const indexOne = serviceEmploye.findIndex(obj => obj._id == preferences[t].service);
        if (indexOne === -1) continue;        
        const indexTwo = serviceEmploye[indexOne].employes.findIndex(obj => obj._id == preferences[t].employe);           
        if (indexTwo !== -1) {
            let pref = {};
            pref.client = idUser;
            pref._id=preferences[t]._id;
            pref.employe = serviceEmploye[indexOne].employes[indexTwo];
            pref.service = serviceEmploye[indexOne];
            preferenceActif.push(pref);
        }
    }
    return preferenceActif;
}

async function listeServiceWithEmploye(service, employe) {
    for (let t in service) {
        if (service[t].employes == undefined)
            continue;
        let tab = [];
        for (let i in service[t].employes) {
            const index = employe.findIndex(obj => obj._id == service[t].employes[i]);
            if (index !== -1)
                tab.push(employe[index]);
        }
        service[t].employes = tab;        
    }    
    return service;
}

exports.listPreferenceDispo = listPreferenceDispo;