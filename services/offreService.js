const {CONTEXT_PATH}=require("../env");
const MailSender=require('../models/MailSender');
const Client=require('../models/Client');
const moment = require('moment');

async function offreMail(offre){
    let debut=new Date(offre.debut);
    let fin=new Date(offre.fin);
    let debutS=moment(debut).format('DD-MM-YYYY');
    let finS=moment(fin).format('DD-MM-YYYY');
    let client = await Client.find();
    let message="Bonjour,\n\t"+
    `L'offre spéciale sur ${offre.designation} sera disponible dans notre salon du`+
    ` ${debutS} au ${finS}. \n\t`+`Pour plus d'information consulter notre site: `+CONTEXT_PATH;
    for(let t in client){
        console.log(client[t])
        MailSender.mailSend(client[t].email,"Initialisation mots de passe",message);        
    }
}

exports.offreMail=offreMail;
