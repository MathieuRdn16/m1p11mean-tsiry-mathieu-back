const express = require('express');
const { authMiddleware, checkManagerMiddleware } = require('../utils/auth');
const { generatePayload, calculBenefices, paginateArray } = require('../utils/util');
const RendezVous = require('../models/RendezVous');
const Depense = require('../models/Depense');
const Employe = require('../models/Employe');
const router = express.Router();

router.get('/temps-travail', authMiddleware, checkManagerMiddleware,async (req, res) => {
    try {
        const {datemin,datemax,page}=req.query;
        const result =await Employe.getTempsTravail();
        res.status(200).json(generatePayload(result));
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json(generatePayload(error = error));
    }
})

router.get('/reservations/jours', authMiddleware, checkManagerMiddleware, async (req, res) => {
    try {
        const {datemin,datemax,page}=req.query;
        const result = await RendezVous.getReservationsJours(datemin,datemax);
        res.status(200).json(generatePayload(paginateArray(result,page,20)));
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json(generatePayload(error = error));
    }
})

router.get('/reservations/mois', authMiddleware, checkManagerMiddleware, async (req, res) => {
    try {
        const {datemin,datemax,page}=req.query;
        const result = await RendezVous.getReservationsMois(datemin,datemax);
        res.status(200).json(generatePayload(paginateArray(result,page,12)));
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json(generatePayload(error = error));
    }
})

router.get('/ca/jours', authMiddleware, checkManagerMiddleware, async (req, res) => {
    try {
        const {datemin,datemax,page}=req.query;
        const result = await RendezVous.getCaJours(datemin,datemax);
        res.status(200).json(generatePayload(paginateArray(result,page,20)));
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json(generatePayload(error = error));
    }
})

router.get('/ca/mois', authMiddleware, checkManagerMiddleware, async(req, res) => {
    try {
        const {datemin,datemax,page}=req.query;
        const result = await RendezVous.getCaMois(datemin,datemax);
        res.status(200).json(generatePayload(paginateArray(result,page,12)));
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json(generatePayload(error = error));
    }
})

router.get('/benefices', authMiddleware, checkManagerMiddleware,async (req, res) => {
    try {
        const {datemin,datemax,page}=req.query;
        const ca=await RendezVous.getCaMois(datemin,datemax);
        const depenses=await Depense.getDepensesMois(datemin,datemax);
        const result = calculBenefices(ca,depenses,page,12);
        res.status(200).json(generatePayload(result));
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json(generatePayload(error = error));
    }
})

module.exports = router;