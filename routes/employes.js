const express=require('express');
const router=express.Router();
const Employe=require('../models/Employe');
const crypto = require('crypto');
const auth=require('../utils/auth.js');
const bcrypt=require('bcrypt');
const {CONTEXT_PATH} = require('../env.js');
const MailSender=require('../models/MailSender');
const ErrorException=require('../utils/ErrorException');
const { EMPLOYE } = require('../utils/Constante.js');
const { generatePayload, getDefaultSchedule, getWeekDayFromDate, isDateBetween, makeQueryRendezVous, makeQueryRendezVousEmploye } = require('../utils/util.js');
const Service = require('../models/Service.js');
const RendezVous = require('../models/RendezVous.js');

router.get('/',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{
    let search=req.query.search;
    if(search==undefined)search=''        
    const query = (search === '') ? { 'delete': { $in: [0, null] } }  : {
        $or: [
          { 'nom': { $regex: '.*' + search + '.*', $options: 'i' } },
          { 'prenoms': { $regex: '.*' + search + '.*', $options: 'i' } },
          { 'email': { $regex: '.*' + search + '.*', $options: 'i' } },
        ],
        $and: [            
            { 'delete': { $in: [0, null] } } 
          ]
    };
    Employe.find(query).then((data)=>{       
        res.json(data)
    }).catch((error)=>{        
        res.status(401).json({message:error.errors});
        })
})


router.post('/',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{    
    const data=req.body;
    data.date_insertion=new Date();    
    data.horairesTravail=getDefaultSchedule();
    Employe.create(data).then((data)=>{
        const token=auth.generateToken(data,EMPLOYE);  
        const message=CONTEXT_PATH+"/authentification/mdp-employe?login="+token;        
        MailSender.mailSend(data.email,"Initialisation mots de passe",auth.messageInscri(message));
        res.json(data)
    }).catch((error)=>{
            next(error,res);
        })
})
router.use('/', (err, req, res, next) => {
    const error=ErrorException.getMessageValidatorInsertion(err);
    res.status(401).json({message:error});
  });

router.post('/connexion',(req,res,next)=>{       
    const {email,mdp}=req.body; 
    if(mdp==null) 
        res.status(401).json({message:"Adresse email ou mots de passe incorrect"});             
    Employe.findOne({email:email,'delete': { $in: [0, null] }}).then((data)=>{   
        let mdpOk=false;
        if(data!=null)
            mdpOk= bcrypt.compareSync(mdp, data.mdp);        
        if(data==null)
            res.status(401).json({message:"Adresse email incorrect"});                
        else if (!mdpOk)
            res.status(401).json({message:"Mots de passe incorrect"});
        else {
            const token=auth.generateToken(data,EMPLOYE);
            delete data.mdp;
            data.typeUser=EMPLOYE;
            res.json({data:token});
        }        
    }).catch((error)=>{     
        console.error(error);           
        res.status(401).json({message:error.errors});
    })
})


router.put('/',auth.authMiddleware,auth.checkEmployeMiddleware,(req,res,next)=>{
    const data=req.body;            
    Employe.updateOne(
        {_id:data._id},
        {$set:
            {nom:data.nom,
                prenoms:data.prenoms,
                email:data.email                
            }},{ runValidators: true}
    ).then((data)=>{
        res.json({token:auth.generateToken(data,EMPLOYE)})
    }).catch((error)=>{        
        next(error,res);
        })
})

router.put('/mdp',auth.authMiddleware,auth.checkEmployeMiddleware,(req,res,next)=>{
    const data=req.body;
    const token = req.headers['authorization'];;
    const user=auth.verifyToken(token);
    if(user==false) 
        return res.status(401).json({message:"Token invalide, veuillez contacter votre Manager"});                
    data.mdp=auth.cryptage(data.mdp);
    Employe.updateOne(
        {_id:user._id},
        {mdp:data.mdp}
    ).then((data)=>{
        res.json(data)
    }).catch((error)=>{
        console.log(error)
        res.status(401).json({message:error.errors});
        })
})

router.get('/mdp',(req,res,next)=>{  
    const email=req.query.email;  
    Employe.findOne({email:email,'delete': { $in: [0, null] }}).then((data)=>{           
        if(data==null)
            res.status(401).json({message:"Adresse email incorrect"});                
        else {            
            const token=auth.generateToken(data,EMPLOYE);  
            const message=CONTEXT_PATH+"/authentification/mdp-employe?login="+token;        
            MailSender.mailSend(data.email,"Réinitialisation mots de passe",auth.messageOublieMdp(message));                      
            res.json({message:"Un email vous a été envoyer"});
        }        
    }).catch((err)=>{        
        res.status(401).json({message:err.errors});
    })
})

router.delete('/',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{
    if(req.query.id==undefined ) 
        res.status(401).json({message:"Utilisateur non valide"});                
    const data={};
    data._id=req.query.id;
    data.delete=1;            
    Employe.updateOne(
        {_id:data._id},
        {$set:data}
    ).then((data)=>{
        res.json(data)
    }).catch((error)=>{
        res.status(401).json({message:error.errors});
        })
})

router.get('/schedule',auth.authMiddleware,auth.checkEmployeMiddleware,(req,res,next)=>{
    const user=req.user;
    Employe.findOne({ _id: user._id},'horairesTravail').then((data)=>{
        console.log("DATA=",data);
        let schedule=getDefaultSchedule();
        schedule={
            ...schedule,
            ...data.horairesTravail
        }
        return res.status(200).json(generatePayload(schedule));
    }).catch((error)=>{
        res.status(400).json({message:error.message})
    });
});

router.post('/schedule',auth.authMiddleware,auth.checkEmployeMiddleware,(req,res,next)=>{
    try {
        const user=req.user;
        const schedule=req.body.schedule;
console.log(schedule);
        let resp_data={};
        Employe.findByIdAndUpdate(user._id,{
            $set:{
                "horairesTravail":schedule
            }
        },{new:true}).then((data)=>{
            resp_data.data=data
        }).catch((err)=>{
            resp_data.error=err.message;
        }).finally(()=>{
            res.status(200).json(resp_data);
        })
    } catch (error) {
        console.error(error);
        res.status(200).json(generatePayload(error=error.message));
    }
});

router.get('/free', async (req, res, next) => {
    try {
        console.log(req.query.date);
        const date = new Date(req.query.date);
        console.log(date);
        const idService = req.query.idService;
        const service = await Service.findById({ _id: idService }).populate({ path: 'employes', select: '_id horairesTravail nom prenoms' }).exec();
        console.log(service);
        let dateRdvFin = new Date(date);
        dateRdvFin.setMinutes(date.getMinutes() + service.duree);
        const empsWorking = [];
        for (let i = 0; i < service.employes.length; i++) {
            const emp=service.employes[i];
            if (emp.isFree(date,dateRdvFin)) {
                let temp={...emp.toObject()};
                delete temp.horairesTravail;
                empsWorking.push(temp)
            }
        }
        // setTimeout(()=>{
            res.status(200).json(generatePayload(empsWorking));
        // },3000);
    } catch (error) {
        console.error(error);
        res.status(400).json({message:error.message});
    }
});


function getAggregateQuery(query){
    return [
        {
            $lookup:{
                from:'clients',
                localField:'client',
                foreignField:'_id',
                as:'client'
            }
        },
        {
            $match:query
        },
        {
          $project: {
            _id:1,
            date:1,
            service:1,
            etat:1,
            employe:1,
            'client.nom': 1, 
            'client.prenoms': 1, 
            'client._id': 1,
          },
        },
    ]
}

router.get('/rendezvous/planning',auth.authMiddleware,auth.checkEmployeMiddleware, async (req, res) => {
    try {
        const npp = 10;
        let page=req.query.page?req.query.page:1;
        const query=makeQueryRendezVousEmploye(req,req.user._id);
        const aggrQuery=getAggregateQuery(query);
        let rendezvous = await RendezVous.aggregate(aggrQuery)
        .sort({['date']:-1})
        .skip((page-1)*npp)
        .limit(npp);
        const aggreCount=[
            ...aggrQuery,
            {
                $group:{
                    _id:null,
                    totalCount:{$sum:1},
            }}
        ];
        let totalOccurence=await RendezVous.aggregate(aggreCount);
        totalOccurence=totalOccurence[0]?.totalCount||0;
        let data={
            rendezvous:rendezvous,
            page:page,
            totalOccurence:totalOccurence,
            totalPage:Math.floor(totalOccurence/npp)+1
        }
        res.status(200).json(generatePayload(data));
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json(generatePayload(error = error))
    }
});


router.get('/rendezvous/:id',auth.authMiddleware,auth.checkEmployeMiddleware,async (req,res)=>{
    try {
        const rdv=await RendezVous.findRdvWithOffres(req.params.id,'client');
        res.status(200).json(generatePayload(rdv))
    } catch (error) {
        console.error(error);
        console.log("test");
        res.status(error.status||500).json({message:manageError(error)});
    }
});

module.exports=router;