const express=require('express');
const router=express.Router();
const TypeDepense=require('../models/TypeDepense');

router.get('/',(req,res,next)=>{
    TypeDepense.find().then((data)=>{        
        res.json(data);
    }).catch((error)=>{        
        res.status(401).json({message:error.errors});
    })
})

router.post('/',(req,res,next)=>{
    TypeDepense.create(req.body).then((data)=>{        
        res.json(data);
    }).catch((error)=>{        
        res.status(401).json({message:error.errors});
    })
})

module.exports=router;