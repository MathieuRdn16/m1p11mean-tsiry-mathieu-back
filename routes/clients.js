const express = require('express');
const router = express.Router();
const Client = require('../models/Client');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const constante = require('../utils/Constante');
const { authMiddleware, checkEmployeMiddleware, generateToken, checkClientMiddleware } = require('../utils/auth');
const Service = require('../models/Service');
const Employe = require('../models/Employe');
const RendezVous = require('../models/RendezVous');
const auth=require('../utils/auth.js');
const {CONTEXT_PATH} = require('../env.js');
const MailSender=require('../models/MailSender');
const { getWeekDayFromDate, getValidationErrors, manageError, generatePayload, makeQueryRendezVous, makeQueryNomEmploye } = require('../utils/util');
const { getMessageValidatorInsertion } = require('../utils/ErrorException');

router.get('/', (req, res, next) => {
    Client.find({}).then((data) => {
        console.log("result:", data)
        res.json(data)
    }).catch((err) => {
        console.error(err);
        res.json({ error: err });
    });
})

router.post('/', async (req, res, next) => {
    try {
        const body = req.body;
        let client = new Client({ ...body });
        console.log(client);
        await client.save();
        res.status(200).json({ "message": "Insertion réussi" })
    } catch (error) {
        console.error(error);
        res.status(400).json({ "message": error.errors })
    }
});

router.post('/login', async (req, res, next) => {
    try {

        const { email, mdp } = req.body
        await Client.findOne({ email }).then((data) => {
            if (data) {
                if (bcrypt.compareSync(mdp, data.password)) {
                    const token = generateToken(data);
                    res.status(200).json({ data: token });
                } else {
                    res.status(400).json({ error: 'Email ou mot de passe incorrect' });
                }
            } else {
                res.status(400).json({ error: 'Email ou mot de passe incorrect' });
            }
        }).catch((err) => {
            console.error(err);
            res.status(400).json({ "error": err.message })
        })
    } catch (error) {
        console.error(error);
        res.status(400).json({ "error": error.message })
    }
})

router.post('/rendezvous', authMiddleware, checkClientMiddleware, async (req, res, next) => {
    try {
        const cli = req.user;
        let idEmploye = req.body.idEmploye;
        let employe = null;
        const service = await Service.findById({ _id: req.body.idService }).select('-employes -images -date_insertion -__v -desactivate');
        const client = await Client.findById({ _id: cli._id })
        let dateRdv = new Date(req.body.date);
        let dateRdvFin = new Date(dateRdv);
        dateRdvFin.setMinutes(dateRdv.getMinutes() + service.duree);
        let rdv = new RendezVous({
            client: cli._id,
            employe: idEmploye,
            service: service,
            date: dateRdv,
            etat: constante.ETAT_CREE
        });
        await rdv.validate();
        employe = await Employe.findById({ _id: idEmploye });
        console.log(employe);
        employe.checkIfIsFree(dateRdv, dateRdvFin);
        rdv = await rdv.save();
        client.updateOne();
        return res.status(200).json({ data: rdv });
    } catch (error) {
        console.error(error);
        return res.status(error.status || 500).json({ message: manageError(error) });
    }
})

function getAggregateQuery(query) {
    return [
        {
            $lookup: {
                from: 'employes',
                localField: 'employe',
                foreignField: '_id',
                as: 'employe'
            }
        },
        {
            $match: query
        },
        {
            $project: {
                _id: 1,
                date: 1,
                service: 1,
                etat: 1,
                client: 1,
                'employe.nom': 1,
                'employe.prenoms': 1,
                'employe._id': 1,
            },
        },
    ]
}

router.get('/rendezvous/historiques', authMiddleware, checkClientMiddleware, async (req, res) => {
    try {
        const npp = 10;
        let page = req.query.page ? req.query.page : 1;
        const query = makeQueryRendezVous(req, req.user._id);
        const aggrQuery = getAggregateQuery(query);
        let rendezvous = await RendezVous.aggregate(aggrQuery)
            .sort({ ['date']: -1 })
            .skip((page - 1) * npp)
            .limit(npp);
        const aggreCount = [
            ...aggrQuery,
            {
                $group: {
                    _id: null,
                    totalCount: { $sum: 1 },
                }
            }
        ];
        let totalOccurence = await RendezVous.aggregate(aggreCount);
        totalOccurence = totalOccurence[0]?.totalCount || 0;
        let data = {
            rendezvous: rendezvous,
            page: page,
            totalOccurence: totalOccurence,
            totalPage: Math.floor(totalOccurence / npp) + 1
        }
        res.status(200).json(generatePayload(data));
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json(generatePayload(error = error))
    }
});

router.get('/rendezvous/:id', authMiddleware, checkClientMiddleware, async (req, res) => {
    try {
        let rdv = await RendezVous.findRdvWithOffres(req.params.id);
        res.status(200).json(generatePayload(rdv))
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json({ message: manageError(error) });
    }
});

router.post('/rendezvous/payer', authMiddleware, checkClientMiddleware, async (req, res) => {
    try {
        console.log(req.body);
        const { idRendezVous, numero, methodpaiement } = req.body;
        console.log("IDREDV=",idRendezVous);
        const rdv = await RendezVous.findRdvWithOffres(idRendezVous);
        RendezVous.updateOne({
            _id: idRendezVous,
        }, {
            $set: {
                etat: constante.ETAT_PAYE,
                montant: rdv.prixFinal,
                commission:rdv.commission,
                methodpaiement:methodpaiement,
                numero:numero
            }
        }).then((data)=>{
            setTimeout(() => {
                res.status(200).json(generatePayload(rdv));
            }, 2000);
        });
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).json({ message: manageError(error) })
    }
})

router.put('/mdp',auth.authMiddleware,auth.checkClientMiddleware,(req,res,next)=>{
    const data=req.body;
    const token = req.headers['authorization'];;
    data.mdp=auth.cryptage(data.mdp);
    Client.updateOne(
        {_id:user._id},
        {mdp:data.mdp}
    ).then((data)=>{
        res.json(data)
    }).catch((error)=>{
        console.log(error)
        res.status(401).json({message:error.errors});
        })
})

router.get('/mdp',(req,res,next)=>{  
    const email=req.query.email;  
    Client.findOne({email:email,'delete': { $in: [0, null] }}).then((data)=>{           
        if(data==null)
            res.status(401).json({message:"Adresse email incorrect"});                
        else {            
            const token=auth.generateToken(data,constante.CLIENT);  
            const message=CONTEXT_PATH+"/authentification/mdp-client?login="+token;        
            MailSender.mailSend(data.email,"Réinitialisation mots de passe",auth.messageOublieMdp(message));                      
            res.json({message:"Un email vous a été envoyer"});
        }        
    }).catch((err)=>{   
        console.log(err)     
        res.status(401).json({message:err.errors});
    })
})

router.delete('/',auth.authMiddleware,auth.checkClientMiddleware,(req,res,next)=>{
    if(req.query.id==undefined ) 
        res.status(401).json({message:"Utilisateur non valide"});                
    const data={};
    data._id=req.query.id;
    data.delete=1;            
    Client.updateOne(
        {_id:data._id},
        {$set:data}
    ).then((data)=>{
        res.json(data)
    }).catch((error)=>{
        res.status(401).json({message:error.errors});
        })
})

router.put('/',auth.authMiddleware,auth.checkClientMiddleware,(req,res,next)=>{
    const data=req.body;            
    Client.updateOne(
        {_id:data._id},
        {$set:
            {nom:data.nom,
                prenoms:data.prenoms,
                email:data.email                
            }},{ runValidators: true}
    ).then((data)=>{
        res.json({token:auth.generateToken(data,constante.CLIENT)})
    }).catch((error)=>{  
        console.log(error)      
        next(error,res);
        })
})




module.exports = router;