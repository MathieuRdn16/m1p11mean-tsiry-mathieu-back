const express=require('express');
const router=express.Router();
const Service=require('../models/Service');
const Employe=require('../models/Employe');
const employeU=require('../utils/employeUtils');
const ErrorException=require('../utils/ErrorException');
const FileStorage=require('../models/FileStorage');
const { generatePayload } = require('../utils/util.js');
const auth=require('../utils/auth.js');

const { upload, uploadMultiple } = require('../middleware/multer')

router.use('/',(err,req,res,next)=>{
    const error=ErrorException.getMessageValidatorInsertion(err);
    res.status(401).json({message:error});
});

router.get('/', async (req,res,next)=>{
    let search=req.query.search;
    if(search==undefined||search=='undefined')search='';
    search.toUpperCase();
    const query = (search == '') ? { 'prix': { $nin: [0, null] } } : {
        $and: [
          { 'designation': { $regex: '.*' + search + '.*', $options: 'i' } },
          { 'prix': { $nin: [0, null] } }
        ]
      };    
      
    let employes= [];
    await Employe.find({'delete': { $in: [0, null] }}).then( (data)=>{        
        employes=data;        
    }).catch((error)=>{        
        console.log(error);
        res.status(401).json({message:error.errors});
        return;
    });    
    Service.find(query).then( async (data)=>{        
        await employeU.employeActif(data,employes);        
        res.json(data);        
    }).catch((error)=>{    
        console.log(error);    
        res.status(401).json({message:error.errors});
    });
}) 

router.get('/:id',async (req,res)=>{
    try {
        const services=await Service.findById({_id:req.params.id});
/*         const employes=await Employe.find({_id:{$in:services.employes},delete:0}).select('-__v -date_insertion -delete -email');
        services.employes=employes; */
        console.log("SERVICE=",services);
        res.status(200).json(generatePayload(data=services));
    } catch (error) {
        console.error(error);
        res.status(400).json(generatePayload('Aucune donnée',error));    
    }
    
});


router.post('/employes',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{
    let emp=req.body;        
    Employe.find({ _id: { $in: emp }}).then((data)=>{
        res.json(data)
    }).catch((error)=>{        
        res.status(401).json({message:error.errors});
        })
})

router.post('/',auth.authMiddleware,auth.checkManagerMiddleware,uploadMultiple,async (req,res,next)=>{ 
    const uploadedFiles = req.files;  
    console.log(uploadedFiles)  
    const data=JSON.parse(req.body.service);
    const mgs = await FileStorage.uploadFile(uploadedFiles,'services');
    data.images=mgs;     
    data.date_insertion=new Date();    
    Service.create(data).then((data)=>{        
        res.json(data)
    }).catch((error)=>{
            next(error,res);
        })
})

router.post('/addImage',auth.authMiddleware,auth.checkManagerMiddleware,uploadMultiple,async (req,res,next)=>{ 
    const uploadedFiles = req.files;      
    const data=JSON.parse(req.body.service);
    const mgs = await FileStorage.uploadFile(uploadedFiles,'services');
    if(data.imagesString==null)
    data.imagesString=mgs
    else if(mgs!=undefined)
        for(let t in mgs)
            data.imagesString.push(mgs[t]);
    
    console.log(data.imagesString); 
    Service.updateOne(
        {_id:data._id},
        {$set:{images:data.imagesString}},{ runValidators: true}
    ).then((data)=>{
        res.json(data)
    }).catch((error)=>{        
        next(error,res);
    })
})

router.post('/deleteImage',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{ 
    const data=req.body;            
    Service.updateOne(
        {_id:data._id},
        {$set:{images:data.imagesString}},{ runValidators: true}
    ).then((data)=>{
        res.json(data)
    }).catch((error)=>{        
        next(error,res);
        })
})

router.put('/',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{
    const data=req.body;            
    Service.updateOne(
        {_id:data._id},
        {$set:
            {designation:data.designation,
                prix:data.prix,
                commission:data.commission,
                duree:data.duree
            }
    },{ runValidators: true}
    ).then((data)=>{
        res.json(data)
    }).catch((error)=>{        
        next(error,res);
        })
})

router.put('/employes',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{
    const data=req.body;            
    Service.updateOne(
        {_id:data._id},
        {$set:{employes:data.employes}},{ runValidators: true}
    ).then((data)=>{
        res.json(data)
    }).catch((error)=>{        
        next(error,res);
        })
})

router.delete('/',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{
    if(req.query.id==undefined ) 
        res.status(401).json({message:"Service non valide"});                
    const data={};
    data._id=req.query.id;
    if(req.query.actif==0)
        data.desactivate=0;            
    else
        data.desactivate=1;
    Service.updateOne(
        {_id:data._id},
        {$set:data}
    ).then((data)=>{
        res.json(data)
    }).catch((error)=>{
        res.status(401).json({message:error.errors});
        })
})

router.get('/forClient', async (req,res,next)=>{    
    let employes= [];
    await Employe.find({'delete': { $in: [0, null] }}).then( (data)=>{        
        employes=data;        
    }).catch((error)=>{        
        console.log(error);
        res.status(401).json({message:error.errors});
        return;
    });    
    Service.find({ 'desactivate': { $nin: [0, null] } } ).then( async (data)=>{        
        await employeU.employeActif(data,employes);        
        res.json(data);        
    }).catch((error)=>{    
        console.log(error);    
        res.status(401).json({message:error.errors});
    });
}) 



module.exports=router;