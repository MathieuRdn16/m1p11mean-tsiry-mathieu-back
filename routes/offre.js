const express=require('express');
const router=express.Router();
const Offre=require('../models/Offre');
const ErrorException=require('../utils/ErrorException');
const oServ=require('../services/offreService');
const auth=require('../utils/auth.js');

router.use('/',(err,req,res,next)=>{
    const error=ErrorException.getMessageValidatorInsertion(err);
    res.status(401).json({message:error});
});


router.get('/',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{
    let search=req.query.search;
    if(search==undefined||search=='undefined')search='';
    search.toUpperCase();
    const query = (search == '') ? { 'delete': { $in: [0, null] } } : {
        $and: [
          { 'designation': { $regex: '.*' + search + '.*', $options: 'i' } },
          { 'delete': { $in: [0, null] } }
        ]
      };    
    Offre.find(query).then((data)=>{                
        res.json(data);        
    }).catch((error)=>{    
        console.log(error);    
        res.status(401).json({message:error.errors});
    });
});

router.post('/',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{
    let of=req.body;
    let total=0;
    let duree=0;
    if(new Date(of.debut)>new Date(of.fin)){
        res.status(401).json({message:"Date de fin antérieure à la date de début"});
        return;
    }
    const now=new Date();
    now.setUTCHours(0, 0, 0, 0);    
    if(new Date(of.debut)<now){
        res.status(401).json({message:"Date de début antérieure à la date d'aujourd'hui"});
        return;
    }
    if(of.services==undefined){
        res.status(401).json({message:"Service obligatoire"});
        return;
    }
    for(let t in of.services){
        duree+=of.services[t].duree;
        total+=of.services[t].prix;
    }
    let pourcent =(total*of.reduction)/100;
    of.prixReduit=total-pourcent;
    of.duree=duree;
    of.prixTotal=total;
    Offre.create(of).then((data)=>{
        oServ.offreMail(data);
        res.json(data)
    }).catch((error)=>{
        next(error,res);
    })
});

router.delete('/',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{
    let id=req.query.id;
    Offre.updateOne(
        {_id:id},
        {$set:{delete:1}},
    ).then((data)=>{
        res.json(data)
    }).catch((error)=>{
        next(error,res);
    }) 
});

module.exports=router;