const express=require('express');
const router=express.Router();
const Manager=require('../models/Manager');
const auth=require('../utils/auth.js');
const bcrypt=require('bcrypt');
const MailSender=require('../models/MailSender');
const ErrorException=require('../utils/ErrorException');
const { MANAGER} = require('../utils/Constante.js');
const {CONTEXT_PATH} = require('../env.js');

router.get('/',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{    
    let search=req.query.search;    
    if(search==undefined)search=''
    search.toUpperCase();
    const query = (search === '') ? { 'delete': { $in: [0, null] } }  : {
        $or: [
          { 'nom': { $regex: '.*' + search + '.*', $options: 'i' } },
          { 'prenoms': { $regex: '.*' + search + '.*', $options: 'i' } },
          { 'email': { $regex: '.*' + search + '.*', $options: 'i' } },
        ],
        $and: [            
            { 'delete': { $in: [0, null] } } 
          ]
      };
    Manager.find(query).then((data)=>{
        res.json(data)
    }).catch((err)=>{
            console.log(err)
            res.status(401).json({message:err.errors});
        })
})

router.post('/connexion',(req,res,next)=>{       
    const login=req.body; 
    if(login.mdp==null) 
        res.status(401).json({message:"Adresse email ou mots de passe incorrect"});            
    Manager.findOne({email:login.email,'delete': { $in: [0, null] }}).then((data)=>{   
        let mdpOk=false
        if(data!=null)
            mdpOk= bcrypt.compareSync(login.mdp, data.mdp);        
        if(data==null)
            res.status(401).json({message:"Adresse email incorrect"});        
        else if(!mdpOk){
            res.status(401).json({message:"Mots de passe incorrect"});
        }
        else {            
            const token=auth.generateToken(data,MANAGER);                        
            res.json({token:token,user:data});
        }        
    }).catch((err)=>{        
        res.status(401).json({message:err.errors});
    })
})

router.post('/',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{
    const data=req.body;
    data.date_insertion=new Date(); 
    Manager.create(data).then((data)=>{
        const token=auth.generateToken(data,MANAGER);  
        const message=CONTEXT_PATH+"/authentification/mdp-manager?login="+token;            
        MailSender.mailSend(data.email,"Initialisation mots de passe",auth.messageInscri(message));        
        res.json(data);
    }).catch((error)=>{
        next(error,res);
        })
})

router.use('/', (err, req, res, next) => {    
    const error=ErrorException.getMessageValidatorInsertion(err);
    res.status(401).json({message:error});
  });

router.put('/',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{
    const data=req.body;      
    Manager.updateOne(
        {_id:data._id},
        {$set:
            {nom:data.nom,
                prenoms:data.prenoms,
                email:data.email                
            }},{ runValidators: true}
    ).then((data)=>{
        res.json({token:auth.generateToken(data,MANAGER)})
    }).catch((error)=>{
        next(error,res);
        })
})

router.put('/mdp',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{    
    const token = req.headers['authorization'];;
    const user=auth.verifyToken(token);
    if(user==false) 
        return res.status(401).json({message:"Token invalide, veuillez contacter votre Manager"});
    const data=req.body;
    data.mdp=auth.cryptage(data.mdp);
    Manager.updateOne(
        {_id:user._id},
        {mdp:data.mdp}
    ).then((data)=>{
        res.json(data)
    }).catch((error)=>{        
        res.status(401).json({message:error.errors});
        })
})

router.get('/mdp',(req,res,next)=>{  
    const email=req.query.email;  
    Manager.findOne({email:email,'delete': { $in: [0, null] }}).then((data)=>{           
        if(data==null)
            res.status(401).json({message:"Adresse email incorrect"});                
        else {            
            const token=auth.generateToken(data,MANAGER);  
            const message=CONTEXT_PATH+"/authentification/mdp-manager?login="+token;        
            MailSender.mailSend(data.email,"Réinitialisation mots de passe",auth.messageOublieMdp(message));                      
            res.json({message:"Un email vous a été envoyer"});
        }        
    }).catch((err)=>{        
        res.status(401).json({message:err.errors});
    })
})


router.delete('/',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{
    if(req.query.id==undefined ) 
        res.status(401).json({message:"Utilisateur non valide"});                
    const data={};
    data._id=req.query.id;
    data.delete=1;            
    Manager.updateOne(
        {_id:data._id},
        {$set:data}
    ).then((data)=>{
        res.json(data)
    }).catch((error)=>{
        res.status(401).json({message:error.errors});
        })
})


module.exports=router;