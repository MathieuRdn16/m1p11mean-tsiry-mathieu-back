const express=require('express');
const router=express.Router();
const Depense=require('../models/Depense');
const ErrorException=require('../utils/ErrorException');
const auth=require('../utils/auth.js');

router.use('/',auth.authMiddleware,auth.checkManagerMiddleware, (err, req, res, next) => {    
    const error=ErrorException.getMessageValidatorInsertion(err);
    res.status(401).json({message:error});
  });

router.get('/',(req,res,next)=>{
    Depense.find().sort({ ['date_insertion']: -1 }).then((data)=>{
        res.json(data)
    }).catch((error)=>{
        res.status(401).json({message:error.errors});
    })
})

router.post('/',auth.authMiddleware,auth.checkManagerMiddleware,(req,res,next)=>{
    const data=req.body;    
    Depense.create(data).then((data)=>{
        res.json(data);
    }).catch((error)=>{
        next(error,res);
    })
})

module.exports=router;