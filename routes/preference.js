const express=require('express');
const router=express.Router();
const Preference=require('../models/Preference');
const prefS=require('../services/preferenceService');
const ErrorException=require('../utils/ErrorException');
const Employe = require('../models/Employe');
const auth=require('../utils/auth');

router.use('/',(err,req,res,next)=>{
    const error=ErrorException.getMessageValidatorInsertion(err);
    res.status(401).json({message:error});
});

router.post('/',auth.authMiddleware,auth.checkClientMiddleware,(req,res,next)=>{
    const token=req.headers['authorization'];;
    const user=auth.verifyToken(token);
    if(user==false)
        return res.status(401).json({message:"Veuillez vous reconnecter"});                
    const pref={};
    pref.service=req.query.service;
    pref.employe=req.query.employe;
    pref.client=user._id;
    Preference.create(pref).then((data)=>{
        res.json(data);
    }).catch((error)=>{
        next(error,res);
    })
});

router.get('/',auth.authMiddleware,auth.checkClientMiddleware,async(req,res,next)=>{
    const token=req.headers['authorization'];;
    const user=auth.verifyToken(token);
    if(user==false)
        return res.status(401).json({message:"Veuillez vous reconnecter"});                
    try{
        const preference =await prefS.listPreferenceDispo(user._id);
        res.json(preference);
    }catch(error){
        console.log(error);
        res.status(401).json({message:error.errors});
    };
});

router.delete('/',auth.authMiddleware,auth.checkClientMiddleware,(req,res,next)=>{    
    let id=req.query.id;    
    Preference.updateOne(
        {_id:id},
        {$set:{delete:1}}
    ).then((data)=>{
        res.json(data)
    }).catch((error)=>{        
        next(error,res);
        })
});

module.exports=router;