const express=require('express');
const router=express.Router();
const Service=require('../models/Service');
const Offre=require('../models/Offre');


router.use('/',(err,req,res,next)=>{
    const error=ErrorException.getMessageValidatorInsertion(err);
    res.status(401).json({message:error});
});

async function offreActif(){
    let now= new Date();
    now.setUTCHours(0, 0, 0, 0);        
    const query = {
        $and: [
            {'debut':{$lte:now}},
            {'fin':{$gte:now}},
          { 'delete': { $in: [0, null] } }
        ]
      };    
      return await Offre.find(query);        
}


async function queryService(req) {
    console.log(req.query.prix)
    let  query={'desactivate':{ $in: [0, null] } } ;    
    if(req.query.designation!=undefined&&req.query.designation!='undefined'&&req.query.designation!="") {        
        query.designation={ $regex: '.*' + req.query.designation + '.*', $options: 'i' } ;
    }
    if(req.query.prix!=0&&req.query.prix!="null"){
        query.prix={ $lte:req.query.prix } ;
    }
    if(req.query.duree!=0&&req.query.duree!="null"){   
        query.duree={ $lte:req.query.duree } ;
    }        
    return query;

}

router.get('/',async(req,res,next)=>{
    try{
        let page=req.query.page ? req.query.page : 1;        
        let size=6;
        let offres=await offreActif();       
        let services=await Service.find(await queryService(req))
        .sort({ ['date_insertion']: -1 }) 
        .skip((page - 1) * size) 
        .limit(size); 
        const totalServices = await Service.countDocuments(await queryService(req));
        const nombrePage= Math.ceil(totalServices / size);
        res.json({offres:offres,services:services,nombrePage:nombrePage});
    }catch(error){
        console.log(error);
        next(error,res);
        return; 
    }
});

module.exports=router;